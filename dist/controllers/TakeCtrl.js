"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ContainerController = void 0;
const common_1 = require("@tsed/common");
const swagger_1 = require("@tsed/swagger");
const TakeCreation_1 = require("../models/TakeCreation");
const Take_1 = require("../models/Take");
const uuid_1 = require("uuid");
let ContainerController = class ContainerController {
    createContainer(takeCreation) {
        return __awaiter(this, void 0, void 0, function* () {
            const take = new Take_1.Take();
            take.compas = takeCreation.compas;
            take.gyro = takeCreation.gyro;
            take.lang = takeCreation.lang;
            take.lat = takeCreation.lat;
            take.image = (takeCreation.image && takeCreation.image.length) ? takeCreation.image.substring(0, 100) + '...' : '';
            take.id = uuid_1.v4();
            take.createdAt = new Date();
            take.updatedAt = new Date();
            return take;
        });
    }
};
__decorate([
    common_1.Post('/'),
    swagger_1.Returns(Take_1.Take, {
        code: 201,
    }),
    __param(0, common_1.BodyParams()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [TakeCreation_1.TakeCreation]),
    __metadata("design:returntype", Promise)
], ContainerController.prototype, "createContainer", null);
ContainerController = __decorate([
    common_1.Controller('/takes'),
    swagger_1.Name('Container')
], ContainerController);
exports.ContainerController = ContainerController;
//# sourceMappingURL=TakeCtrl.js.map