import '@tsed/platform-express'
import { Configuration, GlobalAcceptMimesMiddleware, Inject, PlatformApplication } from '@tsed/common'
import bodyParser from 'body-parser'
import compress from 'compression'
import cookieParser from 'cookie-parser'
import methodOverride from 'method-override'

const rootDir = __dirname

@Configuration({
  mount: {
    '/': [`${rootDir}/controllers/**/*Ctrl.ts`],
  },
  swagger: [
    {
      path: '/api-docs',
      operationIdFormat: "%m"
    },
  ],
})
export class Server {
    @Inject()
  private app!: PlatformApplication

  @Inject(Configuration)
  private settings!: Configuration

  public $beforeRoutesInit(): void | Promise<any> {
    console.log(this.settings.mount)
    this.app
      .use(GlobalAcceptMimesMiddleware)
      .use(cookieParser())
      .use(compress({}))
      .use(methodOverride())
      .use(bodyParser.json({
          limit: '100mb'
      }))
      .use(
        bodyParser.urlencoded({
          extended: true,
          limit: '100mb'
        })
      )
  }
}
