import { Gyro } from './Gyro'
import { Required, Property, PropertyType } from '@tsed/common'

export class Take {
  @Required()
  id!: string

  @Required()
  lat!: number

  @Required()
  lang!: number

  @Required()
  @PropertyType(Gyro)
  gyro!: Gyro

  @Property()
  compas!: number

  @Property()
  image!: string

  @Property()
  version!: number

  @Property()
  createdAt!: Date

  @Property()
  updatedAt!: Date
}
