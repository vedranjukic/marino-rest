"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Take = void 0;
const Gyro_1 = require("./Gyro");
const common_1 = require("@tsed/common");
class Take {
}
__decorate([
    common_1.Required(),
    __metadata("design:type", String)
], Take.prototype, "id", void 0);
__decorate([
    common_1.Required(),
    __metadata("design:type", Number)
], Take.prototype, "lat", void 0);
__decorate([
    common_1.Required(),
    __metadata("design:type", Number)
], Take.prototype, "lang", void 0);
__decorate([
    common_1.Required(),
    common_1.PropertyType(Gyro_1.Gyro),
    __metadata("design:type", Gyro_1.Gyro)
], Take.prototype, "gyro", void 0);
__decorate([
    common_1.Property(),
    __metadata("design:type", Number)
], Take.prototype, "compas", void 0);
__decorate([
    common_1.Property(),
    __metadata("design:type", String)
], Take.prototype, "image", void 0);
__decorate([
    common_1.Property(),
    __metadata("design:type", Number)
], Take.prototype, "version", void 0);
__decorate([
    common_1.Property(),
    __metadata("design:type", Date)
], Take.prototype, "createdAt", void 0);
__decorate([
    common_1.Property(),
    __metadata("design:type", Date)
], Take.prototype, "updatedAt", void 0);
exports.Take = Take;
//# sourceMappingURL=Take.js.map