import { Controller, Post, BodyParams, Get, PathParams, QueryParams, Delete } from '@tsed/common'
import { Returns, Name } from '@tsed/swagger'
import { TakeCreation } from '../models/TakeCreation'
import { Take } from '../models/Take'

import { v4 } from 'uuid'

@Controller('/takes')
@Name('Container')
export class ContainerController {
    @Post('/')
    @Returns(Take, {
      code: 201,
    })
    public async createContainer(@BodyParams() takeCreation: TakeCreation): Promise<Take> {
      const take = new Take()
      take.compas = takeCreation.compas
      take.gyro = takeCreation.gyro
      take.lang = takeCreation.lang
      take.lat = takeCreation.lat
      take.image = (takeCreation.image && takeCreation.image.length) ? takeCreation.image.substring(0, 100) + '...' : ''
      
      take.id = v4()
      take.createdAt = new Date()
      take.updatedAt = new Date()

      return take
    }
}