import { Gyro } from './Gyro'
import { Required, Property, PropertyType } from '@tsed/common'

export class TakeCreation {
  @Required()
  lat!: number

  @Required()
  lang!: number

  @Required()
  @PropertyType(Gyro)
  gyro!: Gyro

  @Property()
  compas!: number

  @Property()
  image!: string
}
