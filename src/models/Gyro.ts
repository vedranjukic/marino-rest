import { Required } from '@tsed/common'

export class Gyro {
  @Required()
  x!: number

  @Required()
  y!: number

  @Required()
  z!: number
}
