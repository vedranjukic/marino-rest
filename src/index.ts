import { PlatformExpress } from '@tsed/platform-express'
import { Server } from './Server'

async function start() {
  const containerApiServer = await PlatformExpress.bootstrap(Server, {
    port: process.env.PORT,
    httpsPort: false,
  })

  await containerApiServer.listen()
}

start()
.catch(console.error)
